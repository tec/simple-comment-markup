#+title: Simple Comment Markup
#+author: tecosaur

#+html: <p><img src="https://img.shields.io/badge/Emacs-26.3+-blueviolet.svg?style=flat-square&logo=GNU%20Emacs&logoColor=white">
#+html: <img src="https://img.shields.io/badge/stage-%CE%B1,%20experimental-red?style=flat-square">
#+html: <a href="https://liberapay.com/tec"><img src="https://shields.io/badge/support%20my%20efforts-f6c915?logo=Liberapay&style=flat-square&logoColor=black"></a></p>

I feel like it's pretty natural to sprinkle a little bit of text markup into
code comments. It's unfortunate that they don't benefit from any extra
fontification.

For those that would appreciate a light bit of markup fontification, one can now
~(require 'simple-comment-markup)~.
